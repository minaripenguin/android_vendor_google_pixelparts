# Powershare
DEVICE_FRAMEWORK_COMPATIBILITY_MATRIX_FILE += vendor/google/pixelparts/powershare/framework_compatibility_matrix.xml
PRODUCT_PACKAGES += \
    vendor.lineage.powershare-service.pixel

BOARD_SEPOLICY_DIRS += vendor/google/pixelparts/sepolicy/powershare
